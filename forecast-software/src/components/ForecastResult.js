import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import wind from '../Wind.svg';
import humidity from '../Cloud-Rain-Sun-Alt.svg';


class ForecastResult extends React.Component {

    render() {
        return (
            <div>
                <div>{this.props.forecast.current.temperature}</div>
                <div>{this.props.forecast.current.weather_descriptions}</div>
                <img src={this.props.forecast.current.weather_icons} />
                <div>
                    <img src={wind} className="App-wind" alt="wind" />
                    {this.props.forecast.current.wind_speed}
                </div>

                <div>
                    <img src={humidity} className="App-humidity" alt="humidity" />
                    {this.props.forecast.current.humidity}
                </div>


            </div>
        );
    }
}


const mapStateToProps = (state /*, ownProps */) => {
    return {
        forecast: state.forecastReducer.forecast,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ForecastResult);