import React from 'react';
import ForecastResult from './ForecastResult';
import ForecastTitle from './ForecastTitle';
import ForecastForm from './ForecastForm';

function Forecast() {
    return (
        <div>
            <ForecastTitle />
            <ForecastResult />
            <ForecastForm />
        </div>
    );
}

export default Forecast;
